package com.ecommerce.microcrommerce.controller;

import com.ecommerce.microcrommerce.dao.ProductDAO;
import com.ecommerce.microcrommerce.model.Product;
import com.ecommerce.microcrommerce.model.ProductDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.List;
import java.net.URI;
import java.util.List;


@RestController

public class ProductController {
    @Autowired
    private ProductDAO productDAO;

    @RequestMapping(value = "/Product", method = RequestMethod.GET)
    public String getListProduct(){
        return "example product";
    }

    @GetMapping (value = "/Product/{id}")
    public String viewProduct(@PathVariable int id) {
        return productDAO.findById(id).toString();
    }

    @RequestMapping (value = "/Products", method = RequestMethod.GET)
    public List<Product> viewProduct() {
        return productDAO.findAll();
    }

    @GetMapping(value = "test/produits/{prixLimit}")
    public List<Product> testeDeRequetes(@PathVariable double prixLimit) {
        return productDAO.findByPriceGreaterThan(prixLimit);
    }

    @GetMapping(value = "test/produitsS/{prix}")
    public List<Product> testeDeRequete(@PathVariable double prix) {
        return productDAO.searchProduct(prix);
    }

    @GetMapping(value = "tests/produits/{search}")
    public List<Product> testeDeRequetes(@PathVariable String search) {
        return productDAO.findByNameLike("%"+search+"%");
    }

    @PostMapping (value = "/ProductSet")
    public ResponseEntity<Void> setProduct(@RequestBody Product product) {
        if( productDAO.save(product) == null ) {
            URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(product.getId()).toUri();
            return ResponseEntity.created(location).build();
        }
        return ResponseEntity.noContent().build();

    }

    /*@DeleteMapping (value = "/ProductDelete")
    public void deleteProduct(@RequestBody Product product){
        productDAO.delete(product);
    }

    @PutMapping (value = "/ProduitsPut")
    public void updateProduit(@RequestBody Product product) {

        productDAO.save(product);
    }*/
}
