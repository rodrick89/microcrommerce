package com.ecommerce.microcrommerce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicrocrommerceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicrocrommerceApplication.class, args);
	}

}
