package com.ecommerce.microcrommerce.dao;

import com.ecommerce.microcrommerce.model.Product;
import com.ecommerce.microcrommerce.model.Product;
import com.ecommerce.microcrommerce.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;



import java.util.List;
@Repository
public interface ProductDAO extends JpaRepository<Product, Integer>{
    /*public List<Product> findAll();
    public Product findById(int id);
    public boolean save(Product product);
    public boolean delete(Product product);*/
    Product findById(int id);
    List<Product> findByPriceGreaterThan(double prixLimit);
    List<Product> findByNameLike(String search);

    @Query("SELECT id, name, price FROM Product p WHERE p.price = :prixLimit")
    List<Product>  searchProduct(@Param("prixLimit") double prixLimit);
}
