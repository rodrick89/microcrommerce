package com.ecommerce.microcrommerce.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import java.io.Serializable;

@Entity
//@Table(name = "product")
public class Product implements Serializable {
    @Id
    @GeneratedValue
    private int id;

    @Length(min = 3, max = 20)
    private String name;

    @Min(value = 1)
    private double price;

    //@JsonIgnore
    private double boughtPrice;

    public Product(){}

    public Product(int id, String name, double price, double boughtPrice){
        this.id = id;
        this.name = name;
        this.price = price;
        this.boughtPrice = boughtPrice;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPrice(double price){
        this.price = price;
    }

    public double getPrice(){
        return this.price;
    }

    public double getBoughtPrice() {
        return boughtPrice;
    }

    public void setBoughtPrice(double boughtPrice) {
        this.boughtPrice = boughtPrice;
    }

    @Override
    public String toString(){
        return "Product{"+
                "id=" + id +
                ", name='"+ name + '\'' +
                ", price=" + price+ '}';
    }


}