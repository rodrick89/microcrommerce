package com.ecommerce.microcrommerce.model;


import com.ecommerce.microcrommerce.dao.ProductDAO;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ProductDaoImpl  {


    public static List<Product> products = new ArrayList<>();

    static {
        products.add(new Product(1, new String("computer"), 350, 300));
        products.add(new Product(2, new String("Aspirator"), 500, 450));
        products.add(new Product(3, new String("Ping Pong Table"), 750, 700));
    }

    //@Override
    public List<Product> findAll() {
        return this.products;
    }

    //@Override
    public Product findById(int id) {
        for (Product product : products) {
            if (product.getId() == id) return product;
        }
        return null;
    }

    //@Override
    public boolean save(Product product) {
        return products.add(product);
    }

    //@Override
    public boolean delete(Product product) {
        if (products.contains(product)) {
            products.remove(product);
            return true;
        } else {
            return false;
        }
    }
}
